var sw;
// Initialize Firebase
var config = {
  apiKey: "AIzaSyADSjZwflVYYvffvjjPyv62-335cZN-ZZM",
  authDomain: "appweb-95b25.firebaseapp.com",
  databaseURL: "https://appweb-95b25.firebaseio.com",
  storageBucket: "appweb-95b25.appspot.com",
  messagingSenderId: "536086321348"
};
firebase.initializeApp(config);


//Auth Email

$('#btnLog').click(function(event) {
  var email = $('#txtEmail').val();
  var pass = $('#txtPass').val();
  var auth = firebase.auth();

  auth.signInWithEmailAndPassword(email, pass).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    console.log('error ('+errorCode+'): '+errorMessage+'_');
  });

});

$('#btnReg').click(function(event) {
  var email = $('#txtEmail').val();
  var pass = $('#txtPass').val();
  var auth = firebase.auth();

  auth.createUserWithEmailAndPassword(email, pass).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    console.log('error ('+errorCode+'): '+errorMessage+'_');
  });

});



$('#btnLogout').click(function(event) {
  firebase.auth().signOut();
});

var json;

firebase.auth().onAuthStateChanged(firebaseUser => {
  if (firebaseUser) {
    $('#btnLogout').removeClass('hide light-blue darken-1');
    $('#btnLogout').addClass('red lighten-1');
    $('#btnLog').addClass('hide');
    var prepa = JSON.stringify(firebaseUser);
    json = JSON.parse(prepa);
    console.log(json);

    $('.userView .imagen').attr('src', json.photoURL);
    $('.userView .name').text(json.displayName);
    $('.userView .email').text(json.email);

    // acciones de relatime data base
    var tit = $('#titulo');
    var refira = firebase.database().ref().child('text');
    refira.on('value', snap => tit.text(snap.val()));


    var storageRef = firebase.storage().ref("subidas/20161226_182230.jpg");
    storageRef.getDownloadURL().then(function(url) {
      console.log(url);
    });

    var referencias = firebase.database().ref().child('Cartas/Efecto');
    var json_cart;
    referencias.on('value' ,  snap => {
      $('#Objetivo').text(JSON.stringify(snap.val(), null , 3));
      json_cart = snap.val();

      var template = '<li class="col s12 m6 l4" >'+
          '<div class="card">'+
            '<div class="card-image">'+
              '<img src=":image:">'+
              '<span class="card-title" style="background: rgba(0,0,0,0.3);">:title:</span>'+
            '</div>'+
            '<div class="card-content">'+
              '<p>:desc:</p>'+
            '</div>'+
            '<div class="card-action">'+
              '<p>Atk: :atk:</p>'+
              '<p>Def: :def:</p>'+
            '</div>'+
          '</div>'+
        '</li>';

      for(var k in json_cart) {
          var cart = template
                        .replace(':title:' , json_cart[k].Nombre)
                        .replace(':desc:' , json_cart[k].Desc)
                        .replace(':image:' , json_cart[k].Imagen)
                        .replace(':atk:' , json_cart[k].Atk)
                        .replace(':def:' , json_cart[k].Def);


          $('#Cartas').append(cart);
      }


    });

    $('#btn-agrega').click(function(event) {
      firebase.database().ref('Cartas/Efecto/99365553').set({
      "Atk" : 2500,
      "Atributo" : "Luz",
      "Def" : 1500,
      "Desc" : "Puedes Invocar esta carta de Modo Especial desde tu mano desterrando 1 monstruo de LUZ y 1 monstruo de OSCURIDAD en tu Cementerio. Puedes Invocar esta carta de Modo Especial desde tu Cementerio mandando al Cementerio 1 monstruo de LUZ y 1 monstruo de OSCURIDAD en tu mano. Cuando esta carta es mandada al Cementerio desde el Campo: puedes seleccionar 1 monstruo de Tipo Dragón de OSCURIDAD de Nivel 5 o mayor en tu Cementerio; Invoca ese objetivo de Modo Especial.",
      "Imagen" : "http://vignette2.wikia.nocookie.net/yugiohenespanol/images/2/2c/Foto_drag%C3%B3n_pulsardeluz.jpg/revision/latest?cb=20160122024453&path-prefix=es",
      "Nivel" : 6,
      "Nombre" : "Lightpulsar Dragon",
      "Tipo" : "Dragón"
      });
    });


    /*
    var dbreflist = referencias.child('Efecto');
    dbreflist.on('child_added' , snap => {
      $('#list').append('<li id="'+snap.key+'">'+snap.val()+'</li>');
    });
    dbreflist.on('child_changed' , snap => {
      var id = snap.key;
      $('#'+id).text(snap.val());
    });
    dbreflist.on('child_removed' , snap => {
      var id_remove = snap.key;
      $('#'+id_remove).remove();
    }); */

  }else{
    console.log('Not Logged in');
    $('#btnLogout').addClass('hide');
    $('#btnLog').removeClass('hide');
    $('.userView .imagen').attr('src', 'images/logopum.png');
    $('.userView .name').text('Usuario');
    $('.userView .email').text('nameuser@gmail.com');
    $('#plano').replaceAll('<div class="row"><div class="col s12"><h1 id="titulo"></h1><pre id="Objetivo"></pre><ul id="list"></ul></div></div>')
    json = null;
    $('#Cartas').replaceAll('');
  }
});

$('#imprime').click(function(event) {
  console.log(json);
});


// Auth Gmail

var user;

/*
$('#Google').click(function(event) {

  var provider = new firebase.auth.GoogleAuthProvider();
  firebase.auth().signInWithPopup(provider).then(function(result) {
  var token = result.credential.accessToken;
  user = result.user;
  }).catch(function(error) {
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
  });
}); */



$('#remember').click(function(event) {
  var auth = firebase.auth();
  var emailAddress = "kabarrera2016@gmail.com";

  auth.sendPasswordResetEmail(emailAddress).then(function() {
    console.log('Correo Enviado');
  }, function(error) {
    console.log(error.message);
  });
});


$('#subir').change(function(event) {
  var file = event.target.files[0]; // get file
  var storageRef = firebase.storage().ref('subidas/'+file.name); // storage ref
  var tarea = storageRef.put(file); // upload file
  tarea.on('state_changed',
    function progress(snapshot) {
      var porcenta = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      $('#uploader').val(porcenta);
    }
  );
});

$('#Google').click(function(event) {
  var provider = new firebase.auth.GoogleAuthProvider();
  firebase.auth().signInWithRedirect(provider);

  firebase.auth().getRedirectResult().then(function(result) {
    if (result.credential) {
      var token = result.credential.accessToken;
    }
    user = result.user;
  }).catch(function(error) {

    var errorCode = error.code;
    var errorMessage = error.message;
    var email = error.email;
    var credential = error.credential;
  });

});




function writeUserData(userId, name, email, imageUrl) {
  firebase.database().ref('users/' + userId).set({
    username: name,
    email: email,
    profile_picture : imageUrl
  });
}

$( document ).ready(function() {
    console.log(json);
});


$('#agregar').click(function(event) {
  writeUserData($('#id').val(), $('#nom').val(), $('#ema').val(), $('#ima').val());
});
